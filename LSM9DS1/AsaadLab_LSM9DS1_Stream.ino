/*****************************************************************
LSM9DS1_Menu.ino
From SFE_LSM9DS1 Library
Original Creation Date: October 11, 2015
Sources from:
https://github.com/sparkfun/LSM9DS1_Breakout

This sketch is designed to stream output from sensors as soon as
it is available through the use of interrupts. PWM outputs
are isolated to stream data for each axis for each sensor.

Hardware setup: The pin-out is as follows:
	LSM9DS1 --------- Arduino
	 SCL ---------- SCL (A5 on older 'Duinos')
	 SDA ---------- SDA (A4 on older 'Duinos')
	 VDD ------------- 3.3V
	 GND ------------- GND
         INT2 ------------- D4
         INT1 ------------- D3
(CSG, CSXM, SDOG, and SDOXM should all be pulled high.
Jumpers on the breakout board will do this for you.)

The LSM9DS1 has a maximum voltage of 3.6V. Make sure you power it
off the 3.3V rail! I2C pins are open-drain, so you'll be
(mostly) safe connecting the LSM9DS1's SCL and SDA pins
directly to the Arduino.

Development environment specifics:
	IDE: Arduino 1.6.3
	Hardware Platform: Arduino Due
	LSM9DS1 Breakout Version: 1.0

Distributed as-is; no warranty is given.
*****************************************************************/

// Include the SparkFunLSM9DS1 library and its dependencies.
#include <Wire.h>
#include <SPI.h>
#include <SparkFunLSM9DS1.h>

LSM9DS1 imu; // Create an LSM9DS1 object

// Interrupt pin definitions
// These can be swapped to any available digital pin:
const int INT1_PIN_THS = 3; // INT1 pin to D3 - will be attached to gyro
const int INT2_PIN_DRDY = 4; // INT2 pin to D4 - attached to accel

// Variable to keep track of when we print sensor readings:
unsigned long lastPrint = 0;

// configureIMU sets up the LSM9DS1 interface, sensor scales
// and sample rates.
uint16_t configureIMU()
{
  // Set up Device Mode (I2C) and I2C addresses:
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.agAddress = LSM9DS1_AG_ADDR(1);
  imu.settings.device.mAddress = LSM9DS1_M_ADDR(1);

  // gyro.latchInterrupt controls the latching of the
  // gyro and accelerometer interrupts (INT1 and INT2).
  // false = no latching
  imu.settings.gyro.latchInterrupt = false;

  // Set gyroscope scale to +/-2000 dps:
  imu.settings.gyro.scale = 2000;
  // Set gyroscope (and accel) sample rate to 119 Hz
  imu.settings.gyro.sampleRate = 3;
  // Set accelerometer scale to +/-8g
  imu.settings.accel.scale = 8;
  // Magnetometer defaults to +/- 4g, 0.625 Hz

  // Call imu.begin() to initialize the sensor and instill
  // it with our new settings.
  return imu.begin();
}

void configureLSM9DS1Interrupts()
{
    // Configure interrupt 2 to fire whenever new accelerometer
    // or gyroscope data is available.
    // Note XG_INT2 means configuring interrupt 2.
    // INT_DRDY_XL is OR'd with INT_DRDY_G
    imu.configInt(XG_INT2, INT_DRDY_XL | INT_DRDY_G, INT_ACTIVE_LOW, INT_PUSH_PULL);
}

void setup()
{
  Serial.begin(115200);
  // Set up our Arduino pins connected to interrupts.
  // We configured all of these interrupts in the LSM9DS1
  // to be active-low.
  pinMode(INT2_PIN_DRDY, INPUT_PULLUP);
  pinMode(INT1_PIN_THS, INPUT_PULLUP);
  // Set up PWM output pins
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(13, OUTPUT);

  // Turn on the IMU with configureIMU() (defined above)
  // check the return status of imu.begin() to make sure
  // it's connected.
  uint16_t status = configureIMU();
  if (!status)
  {
    digitalWrite(13, LOW);
    Serial.print("Failed to connect to IMU: 0x");
    Serial.println(status, HEX);
    while (1) ;
  }

  // After turning the IMU on, configure the interrupts:
  configureLSM9DS1Interrupts();

  // sets resolution used by PWM pins
  analogWriteResolution(12);

  // call calibration function, assumes sensor is facing up
  imu.calibrate(true);
}

void loop()
{
  // INT2 fires when new accelerometer or gyroscope data
  // is available.
  // It's configured to be active LOW:
  if (digitalRead(INT2_PIN_DRDY) == LOW)
  {
    // We don't know if accelerometer or gyroscope data is
    // available.
    // Use accelAvailable and gyroAvailable to check, then
    // read from those sensors if it's new data.
    if (imu.accelAvailable())
      streamAccel();
    if (imu.gyroAvailable())
      streamGyro();
    Serial.println(" ");
    }
}

// Stream the last read accelerometer values:
void streamAccel()
{
  digitalWrite(13, HIGH);
  imu.readAccel();
  {
    // defines values to be used in PWM calculation
    double m = 4095. / (8. + 8.);
    double yint = -(m * -8.);

    // PWM calculation works by dividing the bit resolution by
    // the absolute value of the min and max scale settings
    // such that 0 bits corresponds to min and 4095 bit corresponds
    // to max; this value is m
    // m is then used as a scaling factor when calculating the duty
    // cycle (percent of time the signal is HIGH compared to LOW)
    // biases obtained in the calibration are subtracted from the
    // calculated values
    // analogWrite then tells the PWM pins to output an analog signal
    // with the previously calculated duty cycle

    // AX
    Serial.print(imu.calcAccel(imu.ax));
    double duty_cycle_x = m * (imu.calcAccel(imu.ax)) + yint;
    analogWrite(5, duty_cycle_x);
    Serial.print(" ");

    // AY
    Serial.print(imu.calcAccel(imu.ay));
    double duty_cycle_y = m * (imu.calcAccel(imu.ay)) + yint;
    analogWrite(6, duty_cycle_y);
    Serial.print(" ");

    // AZ
    Serial.print(imu.calcAccel(imu.az));
    double duty_cycle_z = m * (imu.calcAccel(imu.az)) + yint;
    analogWrite(7, duty_cycle_z);
    Serial.print(" ");

    Serial.println(" ");
  }
}

// Stream the last read gyroscope values:
void streamGyro()
{
  digitalWrite(13, HIGH);
  imu.readGyro();
  {
    // defines values to be used in PWM calculation
    double m = 4095. / (2000. + 2000.);
    double yint = -(m * -2000.);

    // GX
    Serial.print(imu.calcGyro(imu.gx));
    double duty_cycle_x = m * (imu.calcGyro(imu.gx)) + yint;
    analogWrite(8, duty_cycle_x);
    Serial.print(" ");

    // GY
    Serial.print(imu.calcGyro(imu.gy));
    double duty_cycle_y = m * (imu.calcGyro(imu.gy)) + yint;
    analogWrite(9, duty_cycle_y);
    Serial.print(" ");

    // GZ
    Serial.print(imu.calcGyro(imu.gz));
    double duty_cycle_z = m * (imu.calcGyro(imu.gz)) + yint;
    analogWrite(10, duty_cycle_z);
    Serial.print(" ");
  }
}
